import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
// import Card from "./components/global-local-recursive/Card.vue";

createApp(App)
  // .component("Card", Card) // 注册为分局组件
  .mount("#app");
