type TreeNode = {
  name: string;
  checked: boolean;
  children?: TreeNode[];
};
